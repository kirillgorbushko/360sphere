//  iOS 360° Player
//  Copyright (C) 2015  Giroptic
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>

#import "GiropticGyroscopeViewController.h"

@interface GiropticGyroscopeViewController ()

@property (weak, nonatomic) IBOutlet UIView *contentHolderView;
@property (weak, nonatomic) IBOutlet UIImageView *tutorialImageView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet GiropticSwitchView *giroSwitch;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpaceImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionTextTopSpace;

@property (strong, nonatomic) NSArray *animationImagesForActiveGyroState;
@property (strong, nonatomic) NSArray *animationImagesForInactiveGyroState;

@end

@implementation GiropticGyroscopeViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self performUILocalization];
    [self prepareDataSourceForAnimations];
    [self updateUIForInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
    self.giroSwitch.delegate = (id)self;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self.giroSwitch updateView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.giroSwitch setOn:self.currentGyroState animated:YES];
    [self registerForNotification];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self startAnimation];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.tutorialImageView stopAnimating];
    [self removeNotifications];
}

#pragma mark - Rotation

- (void)canRotate
{
    //dummy
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    [self updateUIForInterfaceOrientation:toInterfaceOrientation];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
    [self.giroSwitch updateView];
    [self.giroSwitch setOn:self.currentGyroState animated:YES];
}

#pragma mark - IbActions

- (IBAction)closeButtonPress:(id)sender
{
    if (self.didCloseViewController) {
        self.didCloseViewController();
    }

    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - SwitchViewDelegate

- (void)switchViewDidChangeState:(BOOL)switchState
{
    if (self.didSwitchGiro) {
        self.didSwitchGiro(switchState);
    }
    self.currentGyroState = switchState;
    self.descriptionLabel.text = [self localizeDescription];
    [self startAnimation];
}

#pragma mark - Notifications

- (void)registerForNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willEnterForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)removeNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didEnterBackground
{
    [self.tutorialImageView stopAnimating];
}

- (void)willEnterForeground
{
    [self.tutorialImageView startAnimating];
}

#pragma mark - Private

- (void)startAnimation
{
    NSArray *activeScreens = self.animationImagesForInactiveGyroState;
    if (self.giroSwitch.switchState) {
        activeScreens = self.animationImagesForActiveGyroState;
    }
    self.tutorialImageView.image = [activeScreens firstObject];
}

- (void)prepareDataSourceForAnimations
{
    //for demo purpose only
    self.animationImagesForActiveGyroState = @[[UIImage imageNamed:@"active"]];
    self.animationImagesForInactiveGyroState = @[[UIImage imageNamed:@"inactive"]];
}

- (void)updateUIForInterfaceOrientation:(UIInterfaceOrientation)toOrientation
{
    [self.view layoutIfNeeded];
    NSTimeInterval interval = [UIApplication sharedApplication].statusBarOrientationAnimationDuration;
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:interval animations:^{
        CGSize size = [UIScreen mainScreen].bounds.size;
        BOOL sameOrientation = [UIApplication sharedApplication].statusBarOrientation == toOrientation;
        CGFloat value = size.height;
        if (!sameOrientation) {
            value = size.width;
        }
        
        //float values for constant - due to design - no aspect and proportion saved
        if (UIInterfaceOrientationIsPortrait(toOrientation)) {
            weakSelf.topSpaceImageView.constant = value * .17f;
            weakSelf.descriptionTextTopSpace.constant = value * .07f;
        } else {
            weakSelf.topSpaceImageView.constant = value * .12f;
            weakSelf.descriptionTextTopSpace.constant = value * .04f;
        }
        
        [weakSelf.view layoutIfNeeded];
    }];
}

- (void)performUILocalization
{
    self.titleLabel.text = @"Gyroscope";
    self.descriptionLabel.text = [self localizeDescription];
    self.giroSwitch.activeStateTitle = @"Active";
    self.giroSwitch.inactiveStateTitle = @"Inactive";
}

- (NSString *)localizeDescription
{
    NSString *description = @"Touch and drag with your finger to explore the 360 content";
    if (self.currentGyroState) {
        description = @"Move your device to explore the 360 content. Up. Down. All around.";
    }
    return description;
}

@end