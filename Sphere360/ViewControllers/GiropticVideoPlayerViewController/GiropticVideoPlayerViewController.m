//  iOS 360° Player
//  Copyright (C) 2015  Giroptic
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>

#import "GiropticVideoPlayerViewController.H"
#import "GiropticModeSelectionViewController.h"
#import "UINavigationController+RightButton.h"
#import "UINavigationController+Transparent.h"
#import "GiropticGyroscopeViewController.h"

static NSUInteger const GradientViewTag = 100;
static NSInteger const BottomBarInitialHeight = 43;

@interface GiropticVideoPlayerViewController()

@property (weak, nonatomic) IBOutlet GiropticVideoProgressBarView *videoProgressBarView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UILabel *currentProgressLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *downloadingActivityIndicator;

@property (assign, nonatomic) __block SPHViewModel selectedModel;

@end

@implementation GiropticVideoPlayerViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initBarButtons];
    [self addTapGesture];
    [self setControllEnabled:NO];
    [self.downloadingActivityIndicator startAnimating];
    [self.navigationController presentTransparentNavigationBarAnimated:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self prepareGradientForNavigationBar];
    [self prepareGradientForBottomView];
    self.videoProgressBarView.delegate = self;
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self.videoProgressBarView setNeedsToDisplay];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self updateNavigationElements];
}

#pragma mark - Rotation

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    [self prepareGradientForNavigationBar];
    [self.videoProgressBarView setNeedsToDisplay];
}

#pragma mark - SPHVideoPlayerDelegate

- (void)progressDidUpdate:(CGFloat)progress
{
    [super progressDidUpdate:progress];
    
    if (self.isPlaying) {
        [self.downloadingActivityIndicator stopAnimating];
        [self.videoProgressBarView setSliderPosition:progress];
    }
}

- (void)downloadingProgress:(CGFloat)progress
{
    [super downloadingProgress:progress];
    
    [self.videoProgressBarView setBufferingProgresBar:progress];
    if (progress >= (self.playedProgress)) {
        [self.downloadingActivityIndicator startAnimating];
    } else {
        [self.downloadingActivityIndicator stopAnimating];
    }
    
    if ((progress - self.playedProgress) > 0.03) {
        [self setControllEnabled:YES];
        [self.downloadingActivityIndicator stopAnimating];
        if (self.isPlaying) {
            [self.videoPlayer play];
        }
    }
}

- (void)isReadyToPlay
{
    [self setControllEnabled:YES];
    [self playButtonPress:self];
}

- (void)playerDidChangeProgressTime:(CGFloat)time totalTime:(CGFloat)totalDuration
{
    [super playerDidChangeProgressTime:time totalTime:totalDuration];

    NSString *timeString = [NSString stringWithFormat:@"%.2f", time];
    if (time < 9.5 && time > 0 && self.currentProgressLabel.text.length) {
        self.currentProgressLabel.text = [NSString stringWithFormat:@"0%.0f:%@", time, [[timeString componentsSeparatedByString:@"."] lastObject]];
    } else {
        self.currentProgressLabel.text = [NSString stringWithFormat:@"%.0f:%@", time, [[timeString componentsSeparatedByString:@"."] lastObject]];
    }
    NSString *totalTimeString = [NSString stringWithFormat:@"%.2f", totalDuration];
    self.totalTimeLabel.text = [NSString stringWithFormat:@"%.0f:%@", totalDuration, [[totalTimeString componentsSeparatedByString:@"."] lastObject]];
}

- (void)didFailLoadVideo
{
    [[[UIAlertView alloc] initWithTitle:@"Giroptic preview" message:@"Fail to load video" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
}

#pragma mark - GiropticVideoProgressBarViewDelegate

- (void)didChangeProgressTo:(CGFloat)progress
{
    [self.downloadingActivityIndicator startAnimating];
    [self seekPositionAtProgress:progress withPlayingStatus:self.isPlaying];
    if (self.isPlaying) {
        self.playButton.selected = YES;
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self backButtonClickAction:self];
}

#pragma mark - PlayerAction

- (IBAction)playButtonPress:(id)sender
{
    if ([self isPlaying]) {
        [super pauseVideo];
    } else {
        [super playVideo];
    }
    self.playButton.selected = !self.playButton.selected;
}

#pragma mark - IBAction

- (IBAction)backButtonClickAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)tapOnScreen
{
    if (self.navigationController.navigationBarHidden) {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    } else {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
    [self hideBottomBar];
}

- (void)gyroButtonPressed:(UIButton *)sender
{
    GiropticGyroscopeViewController *gyroVC = [self.storyboard instantiateViewControllerWithIdentifier:@"giroViewController"];
    gyroVC.currentGyroState = self.isGyroscopeActive;
    __weak typeof(self) weakSelf = self;
    gyroVC.didSwitchGiro = ^(BOOL state) {
        [weakSelf setGyroscopeActive:state];
    };
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    gyroVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:gyroVC animated:NO completion:nil];
    
//    sender.selected = !sender.selected;
//    if (sender.selected) {
//        [sender setTintColor:[UIColor grayColor]];
//    } else {
//        [sender setTintColor:[UIColor whiteColor]];
//    }
//    [self setGyroscopeActive:sender.selected];
}

- (void)viewModeButtonClickAction:(id)sender
{
    [self performSegueWithIdentifier:@"showViewModeFromVideo" sender:self];
}

#pragma mark - Navigation

- (void)prepareForSegue:(nonnull UIStoryboardSegue *)segue sender:(nullable id)sender
{
    if ([segue.identifier isEqualToString:@"showViewModeFromVideo"]) {
        [self prepareToPresentModeSelectionScreenScreenWithSegue:segue];
    }
}

#pragma mark - Private

- (void)prepareToPresentModeSelectionScreenScreenWithSegue:(UIStoryboardSegue *)segue
{
    [self prepareForPresentingScreen];
    
    GiropticModeSelectionViewController *controller = segue.destinationViewController;
    __weak typeof(self) weakSelf = self;
    void (^SelectModel)(NSInteger model) = ^(NSInteger model){
        if (self.isPlaying) {
            [weakSelf playButtonPress:self];
        }
        [weakSelf switchToModel:model];
        weakSelf.selectedModel = model;
    };
    controller.didSelectMode = ^(SPHViewModel model) {
        SelectModel(model);
        
        if (weakSelf.selectedModel > 0) {
            [((UIBarButtonItem *)[self.navigationItem.rightBarButtonItems lastObject]) setEnabled:NO];
        } else {
            [((UIBarButtonItem *)[self.navigationItem.rightBarButtonItems lastObject]) setEnabled:YES];
        }
        
    };
    controller.didCloseView = ^() {
        [weakSelf hideBottomBar];
        [weakSelf.navigationController setNavigationBarHidden:NO animated:YES];
    };
    controller.activeModel = self.selectedModel;
}

- (void)prepareForPresentingScreen
{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self hideBottomBar];
}

- (void)hideBottomBar
{
    BOOL hidden = self.bottomViewHeightConstraint.constant;
    CGFloat newHeight = hidden ? 0.0f : BottomBarInitialHeight;
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.2 animations:^{
        weakSelf.bottomViewHeightConstraint.constant = newHeight;
        [weakSelf.bottomView layoutIfNeeded];
    }];
}

- (void)initBarButtons
{
    [self.navigationController setButtonsWithImageNamed:@[@"ic_eye_port_2", @"Gyro"]
                                      andActionDelegate:self
                                             tintColors:@[[UIColor whiteColor], [UIColor whiteColor]]
                                               position:ButtonPositionModeRight
                          selectorsStringRepresentation:@[@"viewModeButtonClickAction:", @"gyroButtonPressed:"]
                                            buttonWidth:40.f];
}

- (void)prepareGradientForNavigationBar
{
    [[self.navigationController.navigationBar viewWithTag:GradientViewTag] removeFromSuperview];

    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.navigationController.navigationBar.bounds;
    gradient.colors = @[(id)[[UIColor blackColor] colorWithAlphaComponent:0.3f].CGColor, (id)[UIColor clearColor].CGColor];
    
    UIGraphicsBeginImageContext(self.navigationController.navigationBar.bounds.size);
    [gradient renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImageView *view = [[UIImageView alloc] initWithFrame:self.navigationController.navigationBar.bounds];
    view.image = viewImage;
    view.tag = GradientViewTag;
    [self.navigationController.navigationBar insertSubview:view atIndex:0];
}

- (void)prepareGradientForBottomView
{
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = self.bottomView.bounds;
    gradientLayer.colors = [NSArray arrayWithObjects:(id)[UIColor clearColor].CGColor, (id)[[UIColor blackColor] colorWithAlphaComponent:0.3f].CGColor, nil];
    NSArray *gradientLocations = [NSArray arrayWithObjects:[NSNumber numberWithInt:0.0],[NSNumber numberWithInt:1.0], nil];
    gradientLayer.locations = gradientLocations;
    [self.bottomView.layer insertSublayer:gradientLayer atIndex:0];
}

- (void)addTapGesture
{
    UITapGestureRecognizer *tapOnScreenGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnScreen)];
    [self.view addGestureRecognizer:tapOnScreenGesture];
}

- (void)setControllEnabled:(BOOL)enabled
{
    self.videoProgressBarView.userInteractionEnabled = enabled;
    self.playButton.enabled = enabled;
    self.currentProgressLabel.hidden = !enabled;
    self.totalTimeLabel.hidden = !enabled;
}

- (void)updateNavigationElements
{
    [[self.navigationController.navigationBar viewWithTag:GradientViewTag] removeFromSuperview];
    [self.navigationController presentTransparentNavigationBarAnimated:NO];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}

@end