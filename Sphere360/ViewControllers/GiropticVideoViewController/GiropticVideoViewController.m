//  iOS 360° Player
//  Copyright (C) 2015  Giroptic
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>

#import "GiropticVideoViewController.h"
#import "GiropticVideoPlayerViewController.h"

@interface GiropticVideoViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSMutableArray *videos;
@property (strong, nonatomic) NSString *selectedVideo;

@end

static NSString *const tableViewCellIdentifier = @"videoViewCell";

@implementation GiropticVideoViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareDataSource];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self.navigationController setNavigationBarHidden:NO];
}

#pragma mark - Private

- (void)prepareDataSource
{
    NSArray *names = @[@"254981996"];
    
    if (!self.videos) {
        self.videos = [[NSMutableArray alloc] init];
    }
    for (int i = 0; i < names.count; i++) {
        NSURL *videoName = [[NSBundle mainBundle] URLForResource:names[i] withExtension:@"mp4"];
        [self.videos addObject:[videoName absoluteString]];
    }
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.textLabel.text = [NSString stringWithFormat:@"Video%li", indexPath.row + 1];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.videos count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tableViewCellIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[UITableViewCell alloc] init];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"videoView"]) {
        NSIndexPath *select = [self.tableView indexPathsForSelectedRows][0];
        GiropticVideoPlayerViewController *videoViewController = segue.destinationViewController;
        videoViewController.sourceVideoURL = self.videos[select.row];
    }
}

@end
