//  iOS 360° Player
//  Copyright (C) 2015  Giroptic
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>

#import "GiropticPhotoPlayerViewController.h"
#import "GiropticVideoProgressBarView.h"
#import "GiropticModeSelectionViewController.h"
#import "UINavigationController+RightButton.h"
#import "UINavigationController+Transparent.h"
#import "GiropticPreviewViewController.h"
#import "GiropticGyroscopeViewController.h"

static NSUInteger const GradientViewTag = 100;

@interface GiropticPhotoPlayerViewController()

@property (assign, nonatomic) __block SPHViewModel selectedModel;

@end

@implementation GiropticPhotoPlayerViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initBarButtons];
    [self addTapGesture];
    [self.navigationController presentTransparentNavigationBarAnimated:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
 
    [self prepareGradient];
    
    NSArray *vc = ((UINavigationController *)self.parentViewController).viewControllers;
    if ([vc[vc.count - 2] isKindOfClass:[GiropticPreviewViewController class]]) {
        [self switchToModel:CubicModel];
    }

    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self updateNavigationElements];
}

#pragma mark - Rotation

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    [self prepareGradient];
}

#pragma mark - IBAction

- (IBAction)backButtonClickAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)tapOnScreen
{
    if (self.navigationController.navigationBarHidden) {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    } else {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
}

- (void)gyroButtonPressed:(UIButton *)sender
{
    GiropticGyroscopeViewController *gyroVC = [self.storyboard instantiateViewControllerWithIdentifier:@"giroViewController"];
    gyroVC.currentGyroState = self.isGyroscopeActive;
    __weak typeof(self) weakSelf = self;
    gyroVC.didSwitchGiro = ^(BOOL state) {
        [weakSelf setGyroscopeActive:state];
    };
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    gyroVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:gyroVC animated:NO completion:nil];
    
//    sender.selected = !sender.selected;
//    if (sender.selected) {
//        [sender setTintColor:[UIColor grayColor]];
//    } else {
//        [sender setTintColor:[UIColor whiteColor]];
//    }
//    [self setGyroscopeActive:sender.selected];
}

- (void)viewModeButtonClickAction:(id)sender
{
    [self performSegueWithIdentifier:@"showViewModeFromPhoto" sender:self];
}

#pragma mark - Navigation

- (void)prepareForSegue:(nonnull UIStoryboardSegue *)segue sender:(nullable id)sender
{
    if ([segue.identifier isEqualToString:@"showViewModeFromPhoto"]) {
        [self prepareToPresentModeSelectionScreenScreenWithSegue:segue];
    }
}

#pragma mark - Private

- (void)prepareToPresentModeSelectionScreenScreenWithSegue:(UIStoryboardSegue *)segue
{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    GiropticModeSelectionViewController *controller = segue.destinationViewController;
    __weak typeof(self) weakSelf = self;
    void (^SelectModel)(NSInteger model) = ^(NSInteger model){
        [weakSelf switchToModel:model];
        weakSelf.selectedModel = model;
    };
    controller.didSelectMode = ^(SPHViewModel model) {
        SelectModel(model);
        if (weakSelf.selectedModel) {
            [((UIBarButtonItem *)[self.navigationItem.rightBarButtonItems lastObject]) setEnabled:NO];
        } else {
            [((UIBarButtonItem *)[self.navigationItem.rightBarButtonItems lastObject]) setEnabled:YES];
        }

    };
    controller.didCloseView = ^() {
        [weakSelf.navigationController setNavigationBarHidden:NO animated:YES];
    };
    controller.activeModel = self.selectedModel;
}

- (void)initBarButtons
{
    NSArray *vc = ((UINavigationController *)self.parentViewController).viewControllers;
    if (![vc[vc.count - 2] isKindOfClass:[GiropticPreviewViewController class]]) {
        [self.navigationController setButtonsWithImageNamed:@[@"ic_eye_port_2", @"Gyro"]
                                          andActionDelegate:self
                                                 tintColors:@[[UIColor whiteColor], [UIColor whiteColor]]
                                                   position:ButtonPositionModeRight
                              selectorsStringRepresentation:@[@"viewModeButtonClickAction:", @"gyroButtonPressed:"]
                                                buttonWidth:40.f];
    }
}

- (void)prepareGradient
{
    [[self.navigationController.navigationBar viewWithTag:GradientViewTag] removeFromSuperview];
    
    CAGradientLayer *headerGradient = [CAGradientLayer layer];
    headerGradient.frame = self.navigationController.navigationBar.bounds;
    headerGradient.colors = @[(id)[[UIColor blackColor] colorWithAlphaComponent:0.3f].CGColor, (id)[UIColor clearColor].CGColor];
    
    UIGraphicsBeginImageContext(self.navigationController.navigationBar.bounds.size);
    [headerGradient renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImageView *view = [[UIImageView alloc] initWithFrame:self.navigationController.navigationBar.bounds];
    view.image = viewImage;
    view.tag = GradientViewTag;
    [self.navigationController.navigationBar insertSubview:view atIndex:0];
}

- (void)addTapGesture
{
    UITapGestureRecognizer *tapOnScreenGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnScreen)];
    [self.view addGestureRecognizer:tapOnScreenGesture];
}

- (void)updateNavigationElements
{
    [[self.navigationController.navigationBar viewWithTag:GradientViewTag] removeFromSuperview];
    [self.navigationController presentTransparentNavigationBarAnimated:NO];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}

@end