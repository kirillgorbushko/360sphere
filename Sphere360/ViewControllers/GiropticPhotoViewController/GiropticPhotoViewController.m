//  iOS 360° Player
//  Copyright (C) 2015  Giroptic
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>

#import "GiropticPhotoViewController.h"
#import "GiropticDemoCollectionViewCell.h"
#import "GiropticPhotoPlayerViewController.h"

@interface GiropticPhotoViewController ()

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) NSMutableArray *images;
@property (strong, nonatomic) UIImage *selectedImage;

@end

static NSString *const collectionViewCellIdentifier = @"photoCell";

@implementation GiropticPhotoViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareDataSource];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setHidden:NO];
}

#pragma mark - Private

- (void)prepareDataSource
{
    NSArray *names = @[@"GIR000156.jpg"];
    
    if (!self.images) {
        self.images = [[NSMutableArray alloc] init];
    }
    for (int i = 0; i < names.count; i++) {
        [self.images addObject:[UIImage imageNamed:names[i]]];
    }
}

- (void)configureCell:(GiropticDemoCollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.myImageView.image = self.images[indexPath.row];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.images.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    GiropticDemoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:collectionViewCellIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[GiropticDemoCollectionViewCell alloc] init];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView willSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedImage = self.images[indexPath.row];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize collectionViewSize = self.collectionView.frame.size;
    
    CGFloat width = collectionViewSize.width;
    CGFloat height = (collectionViewSize.height) / 4;
    
    return CGSizeMake(width, height);
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"photoView"]) {
        NSIndexPath *select = [self.collectionView indexPathsForSelectedItems][0];
        GiropticPhotoPlayerViewController *photoViewController = segue.destinationViewController;
        photoViewController.sourceImage = self.images[select.row];
    }
}

@end