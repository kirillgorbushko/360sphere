//  iOS 360° Player
//  Copyright (C) 2015  Giroptic
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>

typedef NS_ENUM(NSUInteger, ButtonPositionMode) {
    ButtonPositionModeLeft,
    ButtonPositionModeRight
};

@interface UINavigationController (RightButton)

- (void)setButtonWithImageNamed:(NSString *)image andActionDelegate:(id)delegate tintColor:(UIColor *)tintColor position:(ButtonPositionMode)position selector:(SEL)buttonSelector;
- (void)setButtonsWithImageNamed:(NSArray *)imageNames andActionDelegate:(id)delegate tintColors:(NSArray *)tintColors position:(ButtonPositionMode)position selectorsStringRepresentation:(NSArray *)buttonSelectors buttonWidth:(CGFloat)buttonWidth;

@end
