//  iOS 360° Player
//  Copyright (C) 2015  Giroptic
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>

@protocol GiropticVideoProgressBarViewDelegate <NSObject>

@optional
- (void)didChangeProgressTo:(CGFloat)progress;

@end

IB_DESIGNABLE
@interface GiropticVideoProgressBarView : UIView

@property (weak, nonatomic) id <GiropticVideoProgressBarViewDelegate> delegate;

@property (strong, nonatomic) IBInspectable UIColor *progressBarBackgroundColor;
@property (strong, nonatomic) IBInspectable UIColor *progressBarBufferingColor;
@property (strong, nonatomic) IBInspectable UIColor *progressBarPlayedColor;
@property (strong, nonatomic) IBInspectable NSString *progressBarPointerImageName;

@property (assign, nonatomic) IBInspectable BOOL isTapEnabled;
@property (assign, nonatomic) IBInspectable BOOL isPanEnabled;

- (void)setBufferingProgresBar:(CGFloat)percent;
- (void)setSliderPosition:(CGFloat)percent;

- (void)setNeedsToDisplay;

@end
