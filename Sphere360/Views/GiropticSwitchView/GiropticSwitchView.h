//  iOS 360° Player
//  Copyright (C) 2015  Giroptic
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>

@protocol GiropticSwitchViewDelegate <NSObject>

@optional
- (void)switchViewDidChangeState:(BOOL)switchState;

@end

IB_DESIGNABLE
@interface GiropticSwitchView : UIView

@property (strong, nonatomic) IBInspectable UIColor *borderColor;
@property (strong, nonatomic) IBInspectable UIColor *onStateColor;
@property (strong, nonatomic) IBInspectable UIColor *offStateColor;
@property (strong, nonatomic) IBInspectable UIColor *switcherColor;
@property (copy, nonatomic) IBInspectable NSString *activeStateTitle;
@property (copy, nonatomic) IBInspectable NSString *inactiveStateTitle;

@property (weak, nonatomic) id <GiropticSwitchViewDelegate> delegate;

@property (assign, nonatomic) BOOL switchState;

- (void)setOn:(BOOL)on animated:(BOOL)animated;
- (void)updateView;

@end
