//  iOS 360° Player
//  Copyright (C) 2015  Giroptic
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>

#import "GiropticSwitchView.h"

static CGFloat const AnimationDuration = 0.25f;

@interface GiropticSwitchView ()

@property (strong, nonatomic) UILabel *switcherLabel;
@property (strong, nonatomic) UILabel *activeLabel;
@property (strong, nonatomic) UILabel *inactiveLabel;

@property (strong, nonatomic) UIPanGestureRecognizer *panGesture;
@property (strong, nonatomic) UITapGestureRecognizer *tapGesture;

@end

@implementation GiropticSwitchView

#pragma mark - Lifecycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self createLabels];
    [self setup];
}

#pragma mark - Public

- (void)updateView
{
    self.activeLabel.text = self.activeStateTitle;
    self.inactiveLabel.text = self.inactiveStateTitle;
    
    if (self.switchState) {
        [self moveSwitcher:self.switcherLabel toX:CGRectGetWidth(self.switcherLabel.bounds) / 2 animated:NO];
    } else {
        [self moveSwitcher:self.switcherLabel toX:CGRectGetWidth(self.bounds) - CGRectGetWidth(self.switcherLabel.bounds) / 2 animated:NO];
    }
}

#pragma mark - Private

- (void)setSwitchState:(BOOL)switchState
{
    _switchState = switchState;
    NSString *title = self.inactiveStateTitle;
    if (switchState) {
        title = self.activeStateTitle;
    }
    self.switcherLabel.text = title;
}

- (void)createLabels
{
    CGRect frame = CGRectMake(0.f, 0.f, CGRectGetWidth(self.bounds) * .5, CGRectGetHeight(self.bounds));
    
    self.switcherLabel = [[UILabel alloc] initWithFrame:frame];
    self.switcherLabel.textAlignment = NSTextAlignmentCenter;
    self.switcherLabel.userInteractionEnabled = YES;
    self.switcherLabel.textColor = [UIColor whiteColor];
    
    self.activeLabel = [[UILabel alloc] initWithFrame:frame];
    self.activeLabel.font = self.switcherLabel.font;
    self.activeLabel.textColor = [UIColor colorWithRed:124/255.0 green:124/255.0 blue:124/255.0 alpha:1];
    self.activeLabel.alpha = 0.7;
    self.activeLabel.textAlignment = NSTextAlignmentCenter;

    frame.origin.x += CGRectGetWidth(self.bounds) / 2;
    self.inactiveLabel = [[UILabel alloc] initWithFrame:frame];
    self.inactiveLabel.font = self.switcherLabel.font;
    self.inactiveLabel.textColor = self.activeLabel.textColor;
    self.inactiveLabel.alpha = 0.7;
    self.inactiveLabel.textAlignment = NSTextAlignmentCenter;

    [self addSubview:self.activeLabel];
    [self addSubview:self.inactiveLabel];
    [self addSubview:self.switcherLabel];
}

- (void)setup
{
    self.panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.switcherLabel addGestureRecognizer:self.panGesture];
    [self addGestureRecognizer:self.tapGesture];
    
    self.switchState = NO;
    
    UIView *borderView = [[UIView alloc] initWithFrame:self.bounds];
    borderView.layer.borderWidth = 1.f;
    borderView.layer.borderColor = self.borderColor.CGColor;
    borderView.layer.cornerRadius = CGRectGetHeight(self.bounds) / 2;
    [self insertSubview:borderView atIndex:0];
    
    self.switcherLabel.layer.backgroundColor = self.switcherColor.CGColor;
    self.switcherLabel.layer.borderWidth = 1.f;
    self.switcherLabel.layer.borderColor = [UIColor colorWithRed:61/255.0 green:115/255.0 blue:153/255.0 alpha:1].CGColor;
    self.backgroundColor = [self colorForCurrentState];
    self.layer.cornerRadius = CGRectGetHeight(self.bounds) / 2;
    self.switcherLabel.layer.cornerRadius = CGRectGetHeight(self.bounds) / 2;
}

- (void)moveSwitcher:(UIView *)view toX:(CGFloat)positionX animated:(BOOL)animated
{
    if (animated) {
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position.x"];
        animation.fromValue = @(view.layer.position.x);
        animation.toValue = @(positionX);
        animation.duration = AnimationDuration;
        [view.layer addAnimation:animation forKey:@"position"];
    }
    view.layer.position = CGPointMake(positionX, view.layer.position.y);
}

- (UIColor *)colorForCurrentState
{
    UIColor *switchColor;
    
    if ([self isOn]) {
        switchColor = self.onStateColor;
    } else {
        switchColor = self.offStateColor;
    }
    return switchColor;
}

- (BOOL)isOn
{
    return _switchState;
}

- (void)setOn:(BOOL)on animated:(BOOL)animated;
{
    CGFloat switcherWidth = CGRectGetWidth(self.switcherLabel.bounds);
    self.switchState = on;
    
    CGFloat xPosition = CGRectGetWidth(self.bounds) - switcherWidth / 2;
    CGFloat activeLabelAlpha = 0.8;

    if (on) {
        xPosition = switcherWidth / 2;
        activeLabelAlpha = 0;
    }
    [self moveSwitcher:self.switcherLabel toX:xPosition animated:animated];
    [UIView animateWithDuration:animated ? AnimationDuration : 0 animations:^{
        self.activeLabel.alpha = activeLabelAlpha;
        self.inactiveLabel.alpha = 0.8 - activeLabelAlpha;
    }];
}

- (void)updateLabels
{
    CGFloat progress = (self.switcherLabel.center.x - CGRectGetWidth(self.switcherLabel.bounds) / 2.5) / CGRectGetWidth(self.switcherLabel.bounds);
    self.activeLabel.alpha = 0.8 * progress;
    self.inactiveLabel.alpha = 0.8 * (1 - progress);
}

#pragma mark - Gesture

- (void)handlePan:(UIPanGestureRecognizer *)recognizer
{
    UIView *switcherView = recognizer.view;
    CGFloat switcherWidth = CGRectGetWidth(switcherView.bounds);

    switch (recognizer.state) {
        case UIGestureRecognizerStateChanged: {
            CGPoint translation = [recognizer translationInView:switcherView];
            
            if (switcherView.center.x + translation.x < switcherWidth / 2 || switcherView.center.x + translation.x > CGRectGetWidth(self.bounds) - switcherWidth / 2) {
                recognizer.view.center = CGPointMake(switcherView.center.x, switcherView.center.y);
            } else {
                recognizer.view.center = CGPointMake(switcherView.center.x + translation.x, switcherView.center.y);
            }
            if (recognizer.view.center.x <= switcherWidth) {
                self.switchState = YES;
            } else {
                self.switchState = NO;
            }
            [self updateLabels];
            [recognizer setTranslation:CGPointZero inView:switcherView];
            break;
        }
        case UIGestureRecognizerStateEnded: {
            BOOL newState = NO;
            if (recognizer.view.center.x <= switcherWidth) {
                newState = YES;
            }
            [self setOn:newState animated:YES];
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(switchViewDidChangeState:)]) {
                [self.delegate switchViewDidChangeState:self.switchState];
            }
            break;
        }
        default:
            break;
    }
}

- (void)handleTap:(UITapGestureRecognizer *)recognizer
{
    [self setOn:![self isOn] animated:YES];
    if (self.delegate && [self.delegate respondsToSelector:@selector(switchViewDidChangeState:)]) {
        [self.delegate switchViewDidChangeState:self.switchState];
    }
}

@end